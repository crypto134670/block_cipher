My name is Pinkie Pie (hello!)
And I am here to say (how you doing?)
I'm gonna make you smile
And I will brighten up you day
It doesn't matter now (what's up?)
If you are sad or blue (howdy?)
'Cause cheering up my friends
Is just what Pinkie's here to do
'Cause I love to make you smile, smile, smile
Yes, I do
It fills my heart with sunshine all the while
Yes, it does
'Cause all I really need's a smile, smile, smile
From these happy friends of mine
I like to see you grin (awesome!)
I would love to see you beam (rock on!)
The corners of your mouth turned up
Is always Pinkie's dream (hoof bump!)
But if you're kind of worried
And your face has made a frown
I'll work real hard, and do my best
To turn that sad frown upside down
'Cause I love to make you grin, grin, grin
Yes, I do
Bust it out from ear to ear, let it begin
Just give me a joyful grin, grin, grin
And you fill me with good cheer
It's true some days are dark and lonely
And maybe you feel sad
But Pinkie will be there to show you that it isn't that bad
There is one thing that makes me happy
And makes my whole life worthwhile
And that's when I talk to my friends and get them to smile
I really am so happy
Your smile fills me with glee
I give a smile, I get a smile
And that's so special to me
'Cause I love to see you beam, beam, beam
Yes, I do
Tell me, what more can I say to make you see
That I do?
It makes me happy when you beam, beam, beam
Yes, it always makes my day
Come on every pony smile, smile, smile
Fill my heart up with sunshine, sunshine
All I really need's a smile, smile, smile
From these happy friends of mine
Come on every pony smile, smile, smile
Fill my heart up with sunshine, sunshine
All I really need's a smile, smile, smile
From these happy friends of mine
Yes, the perfect gift for me (come on every pony smile, smile, smile, fill my)
Is a smile as wide as a mile (heart up with sunshine, sunshine)
To make me happy as can be (all I really need's a smile, smile, smile, from these happy)
Smile, smile, smile, smile, smile (friends of mine)
Come on and smile
Come on and smile