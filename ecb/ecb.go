package ecb

import (
	"block_cipher/pkcs5"
	"crypto/aes"
)

// encryptECB 使用AES-ECB模式加密数据
func EncryptECB(key, plaintext []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// 填充明文数据
	plaintext = pkcs5.Pkcs5Padding(plaintext, block.BlockSize())

	// 分组大小
	blockSize := block.BlockSize()

	// 初始化密文切片
	ciphertext := make([]byte, len(plaintext))

	// 加密每个分组
	for bs, be := 0, blockSize; bs < len(plaintext); bs, be = bs+blockSize, be+blockSize {
		block.Encrypt(ciphertext[bs:be], plaintext[bs:be])
	}

	return ciphertext, nil
}

// decryptECB 使用AES-ECB模式解密数据
func DecryptECB(key, ciphertext []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// 分组大小
	blockSize := block.BlockSize()

	// 初始化明文切片
	plaintext := make([]byte, len(ciphertext))

	// 解密每个分组
	for bs, be := 0, blockSize; bs < len(ciphertext); bs, be = bs+blockSize, be+blockSize {
		block.Decrypt(plaintext[bs:be], ciphertext[bs:be])
	}

	// 去除填充
	plaintext = pkcs5.Pkcs5UnPadding(plaintext)
	return plaintext, nil
}
