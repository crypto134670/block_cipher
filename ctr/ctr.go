package ctr

import (
	"block_cipher/pkcs5"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
)

// encryptCTR 使用CTR模式加密数据
func EncryptCTR(key, plaintext []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// CTR模式不需要IV（初始化向量），但需要一个计数器
	// 计数器可以是一个简单的字节切片，从{0, 0, 0, ...}开始
	counter := bytes.Repeat([]byte{0}, aes.BlockSize)

	// 填充明文数据
	plaintext = pkcs5.Pkcs5Padding(plaintext, aes.BlockSize)

	// 创建CTR加密器
	ciphertext := make([]byte, len(plaintext))
	ctr := cipher.NewCTR(block, counter)
	ctr.XORKeyStream(ciphertext, plaintext)

	return ciphertext, nil
}

// decryptCTR 使用CTR模式解密数据
func DecryptCTR(key, ciphertext []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// 解密时，CTR模式使用相同的计数器（或者说是起始计数器）
	counter := bytes.Repeat([]byte{0}, aes.BlockSize)

	// 创建CTR解密器（注意：CTR模式中加密和解密使用相同的操作）
	original := make([]byte, len(ciphertext))
	ctr := cipher.NewCTR(block, counter)
	ctr.XORKeyStream(original, ciphertext)

	// 去除填充
	original = pkcs5.Pkcs5UnPadding(original)
	return original, nil
}
