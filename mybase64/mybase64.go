package mybase64

import (
	"encoding/base64"
	"log"
)

func Base64Decode(src []byte) []byte {
	encoded := string(src)
	decoded := make([]byte, base64.StdEncoding.DecodedLen(len(encoded)))
	n, err := base64.StdEncoding.Decode(decoded, src)
	if err != nil {
		log.Fatalln("解码失败:", err)
	}
	// 根据返回的n值对切片进行切片操作，去掉多余的零值字节
	decoded = decoded[:n]
	return decoded
}

func Base64Encode(src []byte) []byte {
	return []byte(base64.StdEncoding.EncodeToString(src))
}
