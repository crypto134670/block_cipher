**go语言五种对称加密的实现**

运行程序

```bash
go run main.go
```

当然也可以运行已经编译的可执行文件，二者是等效的

```bash
go build
./block_cipher
```

可以带三类参数 action，newkey，mode

mode可以使用ECB,CBC,CFB,OFB,CTR中的任一个，例如

```bash
./block_cipher mode=CBC
# 当然也可以在main.go里面main函数第一行修改默认值
# 等号前后不要空格
```

action可以使用 all, encrypt, decrypt中的任一个，例如

```bash
./block_cipher action=decrypt
# 默认是all，即先生成密文再解密成明文
```

默认不会生成新的key，除非使用newkey参数

```bash
./block_cipher newkey
# 会生成新的key
```
三个参数互不冲突，先后不影响