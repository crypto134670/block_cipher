package cfb

import (
	"block_cipher/pkcs5"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"io"
)

// encryptCFB 使用CFB模式加密数据
func EncryptCFB(key, plaintext []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// CFB模式需要初始化向量（IV），通常与块大小相同
	iv := make([]byte, aes.BlockSize)
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}

	// 填充明文数据
	plaintext = pkcs5.Pkcs5Padding(plaintext, aes.BlockSize)

	// 创建CFB加密器
	ciphertext := make([]byte, len(plaintext))
	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext, plaintext)

	// 返回IV和密文
	return append(iv, ciphertext...), nil
}

// decryptCFB 使用CFB模式解密数据
func DecryptCFB(key, ciphertext []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// 分离IV和密文
	if len(ciphertext) < aes.BlockSize {
		return nil, errors.New("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	// 创建CFB解密器
	original := make([]byte, len(ciphertext))
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(original, ciphertext)

	// 去除填充
	original = pkcs5.Pkcs5UnPadding(original)
	return original, nil
}
