package genkey

import (
	"block_cipher/mybase64"
	"crypto/aes"
	"crypto/rand"
	"os"
)

func GenKey() {
	//创建AES 128位 密钥
	key := make([]byte, aes.BlockSize)
	if _, err := rand.Read(key); err != nil {
		panic(err) // 如果生成随机字节失败，则panic
	}
	base64Key := mybase64.Base64Encode(key)
	err := os.WriteFile("key.txt", []byte(base64Key), 0644) //所有者可以读写，其他用户仅可读
	if err != nil {
		panic(err) //写入文件失败，不过通常不会发生
	}
}
