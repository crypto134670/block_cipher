package main

import (
	"block_cipher/cbc"
	"block_cipher/cfb"
	"block_cipher/ctr"
	"block_cipher/ecb"
	"block_cipher/genkey"
	"block_cipher/mybase64"
	"block_cipher/ofb"
	"crypto/aes"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	groupingMode := "CTR"
	action := "ALL"
	for _, arg := range os.Args {
		w := strings.Split(strings.ToUpper(arg), "=")
		switch w[0] {
		case "ACTION":
			action = w[1]
		case "MODE":
			groupingMode = w[1]
		case "NEWKEY":
			fmt.Println("Generating new key...")
			genkey.GenKey()
		}
	}
	fmt.Printf("mode=%s, action=%s\n", groupingMode, action)

	//从key.txt读入密钥
	base64Key, err := os.ReadFile("key.txt")
	if err != nil {
		log.Fatalln("Key file 'key.txt' not exists")
	}
	key := mybase64.Base64Decode(base64Key)
	// 截断或填充key到正确的长度（对于AES是16、24或32字节）
	key = key[:aes.BlockSize]

	// 选择分组模式（这里以CBC为例）
	var encryptFunc func([]byte, []byte) ([]byte, error)
	var decryptFunc func([]byte, []byte) ([]byte, error)
	switch groupingMode { // 这里可以是命令行参数、配置文件或其他方式指定的模式
	case "ECB":
		encryptFunc = ecb.EncryptECB
		decryptFunc = ecb.DecryptECB
	case "CBC":
		encryptFunc = cbc.EncryptCBC
		decryptFunc = cbc.DecryptCBC
	case "CFB":
		encryptFunc = cfb.EncryptCFB
		decryptFunc = cfb.DecryptCFB
	case "OFB":
		encryptFunc = ofb.EncryptOFB
		decryptFunc = ofb.DecryptOFB
	case "CTR":
		encryptFunc = ctr.EncryptCTR
		decryptFunc = ctr.DecryptCTR
	default:
		log.Panicf("Unsupported mode: %s\n", groupingMode)
	}

	if action == "ALL" || action == "ENCRYPT" {
		//读入明文
		plaintext, err := os.ReadFile("plaintext.txt")
		if err != nil {
			panic(err)
		}
		// 加密
		ciphertext, err := encryptFunc(key, plaintext)
		if err != nil {
			panic(err)
		}
		base64Ciphertext := mybase64.Base64Encode(ciphertext)
		err = os.WriteFile(fmt.Sprintf("ciphertext-%s.txt", groupingMode), []byte(base64Ciphertext), 0644)
		if err != nil {
			panic(err)
		}
		fmt.Println("Encryption completed.")
	}

	if action == "ALL" || action == "DECRYPT" {
		//读入密文
		base64Ciphertext, err := os.ReadFile(fmt.Sprintf("ciphertext-%s.txt", groupingMode))
		if err != nil {
			log.Fatal(err)
		}
		ciphertext := mybase64.Base64Decode(base64Ciphertext)
		//解密
		decrypted, err := decryptFunc(key, ciphertext)
		if err != nil {
			log.Fatal(err)
		}
		err = os.WriteFile(fmt.Sprintf("result-%s.txt", groupingMode), decrypted, 0644)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Decryption completed.")
	}
}
